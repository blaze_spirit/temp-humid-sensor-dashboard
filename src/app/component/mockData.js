export default {
    sensorData: [
        {
            name: 'Sensor 1',
            temp: 22.15,
            humid: 65,
            desc: 'Front Door',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 2',
            temp: 24.55,
            humid: 70,
            desc: 'Main Hall',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 3',
            temp: 23.45,
            humid: 72,
            desc: 'Multipurpose Hall',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 4',
            temp: 21.00,
            humid: 80,
            desc: 'Reception',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 5',
            temp: 25.55,
            humid: 45,
            desc: 'Store Room',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 6',
            temp: 23.35,
            humid: 62,
            desc: 'Meeting Room',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 7',
            temp: 26.15,
            humid: 67,
            desc: 'Compressor Room',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 8',
            temp: 25.35,
            humid: 68,
            desc: 'Office 1',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 9',
            temp: 21.73,
            humid: 76,
            desc: 'Office 2',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        },
        {
            name: 'Sensor 10',
            temp: 25.13,
            humid: 87,
            desc: 'Pantry',
            macAddr: '01-23-45-67-89-ab-cd-ef'
        }
    ]
}