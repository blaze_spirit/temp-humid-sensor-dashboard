const SERVER_ENDPOINT                  = '/';
const UPDATE_SENSOR_ENDPOINT           = SERVER_ENDPOINT + 'sensor-info';
const SENSOR_READING_ENDPOINT          = SERVER_ENDPOINT + 'sensor-reading';
const AVAILABLE_EXPORT_PERIOD_ENDPOINT = SERVER_ENDPOINT + 'get-available-export-period';
const EXPORT_ENDPOINT                  = SERVER_ENDPOINT + 'export-data';

const SENSOR_ALIAS_NAME_LIST = [
    'sensor_01',
    'sensor_02',
    'sensor_03',
    'sensor_04',
    'sensor_05',
    'sensor_06',
    'sensor_07',
    'sensor_08',
    'sensor_09',
    'sensor_10',
];

const RANGE_DAILY   = 'daily';
const RANGE_WEEKLY  = 'weekly';
const RANGE_MONTHLY = 'monthly';

export default {
    // REST endpoint
    serverEndPoint: SERVER_ENDPOINT,
    updateSensorEndPoint: UPDATE_SENSOR_ENDPOINT,
    sensorReadingEndpoint: SENSOR_READING_ENDPOINT,
    availableExportPeriodEndpoint: AVAILABLE_EXPORT_PERIOD_ENDPOINT,
    exportEndpoint: EXPORT_ENDPOINT,

    sensorAliasNameList: SENSOR_ALIAS_NAME_LIST,

    // constant
    range: {
        daily: RANGE_DAILY,
        weekly: RANGE_WEEKLY,
        monthly: RANGE_MONTHLY
    }
}