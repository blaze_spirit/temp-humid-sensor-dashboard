import Vue       from 'vue';
import VueRouter from 'vue-router';
import Dashboard from '../component/dashboard.vue';
import History   from '../component/history.vue';
import Setting   from '../component/setting.vue';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'dashboard',
        component: Dashboard
    },
    {
        path: '/historical',
        name: 'historical',
        component: History
    },
    {
        path: '/setting',
        name: 'setting',
        component: Setting
    },
    {
        path: '*',
        redirect: '/',
    }
];

export default new VueRouter({
    mode: 'hash',
    routes
});
