import config     from '../config/config.js';
import apiUtils   from './api-utils.js';
import chartUtils from './chart-utils.js';

export default {
    getSensorList,
    getSensorReading
}

function getSensorList() {
    return chartUtils.getSensorList();
}

function getSensorReading() {
    return new Promise((resolve, reject) => {
        apiUtils
        .fetchFromAPI(config.sensorReadingEndpoint)
        .then(response => {
            resolve(response);
        })
        .catch(err => {
            reject(err);
        });
    });
}
