export default {
    fetchFromAPI,
    postToAPI
}

function fetchFromAPI(url) {
    return new Promise((resolve, reject) => {
        fetch(url)
        .then(response => {
            resolve(response.json());
        })
        .catch(err => {
            reject(err);
        });
    });
}

function postToAPI(url, data) {
    let postObj = {
        method: "POST",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    };

    return fetch(url, postObj);
}