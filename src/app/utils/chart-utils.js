import { GoogleCharts } from 'google-charts';
import config           from '../config/config.js';
import apiUtils         from './api-utils.js';

const SENSOR_LIST_URL = config.serverEndPoint + 'sensor-list/';

export default {
    getSensorList,
    getDailyTempData,
    getWeeklyTempData,
    getMonthlyTempData,
    getDailyHumidData,
    getWeeklyHumidData,
    getMonthlyHumidData,
    buildTempDataTable,
    buildHumidDataTable,
    getChartOptions,
    getChartColor
}

function getSensorList() {
    return new Promise((resolve, reject) => {
        apiUtils
        .fetchFromAPI(SENSOR_LIST_URL)
        .then(sensorList => {
            resolve(_sortSensorList(sensorList));
        })
        .catch(err => {
            reject(err);
        });
    });
}

function getDailyTempData() {
    let timeEnd   = Date.now();
    let timeStart = timeEnd - (12 * 60 * 60 * 1000);
    let grouping  = 600000; // 10 minutes

    let apiUrl = config.serverEndPoint + 'temp/' + timeStart + '/' + timeEnd + '/' + grouping;

    return apiUtils.fetchFromAPI(apiUrl);
}

function getWeeklyTempData() {
    let timeEnd   = Date.now();
    let timeStart = timeEnd - (7 * 24 * 60 * 60 * 1000);
    let grouping  = 3600000; // 1 hour

    let apiUrl = config.serverEndPoint + 'temp/' + timeStart + '/' + timeEnd + '/' + grouping;

    return apiUtils.fetchFromAPI(apiUrl);
}

function getMonthlyTempData() {
    let timeEnd   = Date.now();
    let timeStart = new Date();
    let grouping  = 7200000; // 2 hours

    // set timeStart to be start of the month
    timeStart.setDate(1);
    timeStart.setHours(0, 0, 0, 0);
    timeStart = timeStart.getTime();

    let apiUrl = config.serverEndPoint + 'temp/' + timeStart + '/' + timeEnd + '/' + grouping;

    return apiUtils.fetchFromAPI(apiUrl);
}

function getDailyHumidData() {
    let timeEnd   = Date.now();
    let timeStart = timeEnd - (12 * 60 * 60 * 1000);
    let grouping  = 600000; // 10 minutes

    let apiUrl = config.serverEndPoint + 'humid/' + timeStart + '/' + timeEnd + '/' + grouping;

    return apiUtils.fetchFromAPI(apiUrl);
}

function getWeeklyHumidData() {
    let timeEnd   = Date.now();
    let timeStart = timeEnd - (7 * 24 * 60 * 60 * 1000);
    let grouping  = 3600000; // 1 hour

    let apiUrl = config.serverEndPoint + 'humid/' + timeStart + '/' + timeEnd + '/' + grouping;

    return apiUtils.fetchFromAPI(apiUrl);
}

function getMonthlyHumidData() {
    let timeEnd   = Date.now();
    let timeStart = new Date();
    let grouping  = 7200000; // 2 hours

    // set timeStart to be start of the month
    timeStart.setDate(1);
    timeStart.setHours(0, 0, 0, 0);
    timeStart = timeStart.getTime();

    let apiUrl = config.serverEndPoint + 'humid/' + timeStart + '/' + timeEnd + '/' + grouping;

    return apiUtils.fetchFromAPI(apiUrl);
}

// process result data to be compatible with Google Chart Datatable
function buildTempDataTable(sensorList, sensorData) {
    let processedResult = [];

    let dataTable = new GoogleCharts.api.visualization.DataTable();
    dataTable.addColumn('datetime', 'timestamp');

    sensorList.forEach(sensor => {
        dataTable.addColumn('number', sensor.name);
    });

    // populate the data with timestamp as 1st column
    // and pre-fill the table cell with 'null'
    if (sensorData[0]) {
        sensorData[0].rows.forEach(row => {
            let nullDataArray = Array(sensorList.length).fill(null);
            processedResult.push([new Date(row.time)].concat(nullDataArray));
        });
    }

    // populate data.
    sensorData.forEach(series => {
        // find sensor name by alias name
        let sensorName = sensorList.filter(sensor => sensor.alias_name === series.tags.alias_name)[0].name;

        // look for column which match the sensor name
        for (let column = 1; column < dataTable.getNumberOfColumns(); column ++) {
            if (dataTable.getColumnLabel(column) === sensorName) {
                processedResult.forEach((row, index) => {
                    if (series.rows[index].temp === -100) {
                        row[column] = null;
                    }
                    else {
                        row[column] = series.rows[index].temp;
                    }
                });
            }
        }
    });

    dataTable.addRows(processedResult);

    // format tooltip
    let dateFormatter = new google.visualization.DateFormat({ pattern: 'd MMM y, h:mm a' });
    dateFormatter.format(dataTable, 0);

    let readingFormatter = new google.visualization.NumberFormat({suffix: ' °C'});
    for (let i = 1; i <= 10; i++) {
        readingFormatter.format(dataTable, i);
    }
    return dataTable;
}

// process result data to be compatible with Google Chart Datatable
function buildHumidDataTable(sensorList, sensorData) {
    let processedResult = [];

    let dataTable = new GoogleCharts.api.visualization.DataTable();
    dataTable.addColumn('datetime', 'timestamp');

    sensorList.forEach(sensor => {
        dataTable.addColumn('number', sensor.name);
    });

    // populate the data with timestamp as 1st column
    // and pre-fill the table cell with 'null'
    if (sensorData[0]) {
        sensorData[0].rows.forEach(row => {
            let nullDataArray = Array(sensorList.length).fill(null);
            processedResult.push([new Date(row.time)].concat(nullDataArray));
        });
    }

    // populate data.
    sensorData.forEach(series => {
        // find sensor name by alias name
        let sensorName = sensorList.filter(sensor => sensor.alias_name === series.tags.alias_name)[0].name;

        // look for column which match the sensor name
        for (let column = 1; column < dataTable.getNumberOfColumns(); column ++) {
            if (dataTable.getColumnLabel(column) === sensorName) {
                processedResult.forEach((row, index) => {
                    if (series.rows[index].humid === -100) {
                        row[column] = null;
                    }
                    else {
                        row[column] = series.rows[index].humid;
                    }
                });
            }
        }
    });

    dataTable.addRows(processedResult);

    // format tooltip
    let dateFormatter = new google.visualization.DateFormat({ pattern: 'd MMM y, h:mm a' });
    dateFormatter.format(dataTable, 0);

    let readingFormatter = new google.visualization.NumberFormat({suffix: ' %'});
    for (let i = 1; i <= 10; i++) {
        readingFormatter.format(dataTable, i);
    }
    return dataTable;
}

function getChartOptions(hideIndex, rangeFilter) {
    let chartOptions = _getChartOptions();
    let chartColor = getChartColor();

    if (hideIndex && hideIndex.length > 0) {
        hideIndex.forEach(index => {
            chartColor[index] = null; // mark filtered index as 'null'
        });

        chartColor = chartColor.reduce((accumulator, currValue) => { // filter out 'null'
            if (currValue != null) {
                accumulator.push(currValue);
                return accumulator;
            }
            else {
                return accumulator;
            }
        }, []);
    }
    chartOptions.colors = chartColor;

    // hAxis label based on range filter
    if (rangeFilter === config.range.daily) {
        chartOptions.hAxis.format = 'h a';
    }
    else if (rangeFilter === config.range.weekly
             || rangeFilter === config.range.monthly) {
        chartOptions.hAxis.format = 'd MMM';
    }
    
    return chartOptions;
}

function getChartColor() {
    return [
        '#f44336',
        '#f48fb1',
        '#ab47bc',
        '#673ab7',
        '#2196f3',
        '#66bb6a',
        '#ffeb3b',
        '#8d6e63',
        '#ff9800',
        '#1565c0'
    ];
}

function _getChartOptions() {
    return {  
        vAxis: {
            textStyle: {
                color: '#cecece'
            },
            gridlines: {
                color: '#3e525d'
            },
            baselineColor: '#d6d6d6'
        },
        hAxis: {
            format: 'h a',
            textStyle: {
                color: '#cecece'
            },
            gridlines: {
                color: '#3e525d'
            },
            baselineColor: '#ffffff'
        },
        chartArea: {
            width: '93%',
            height: '85%',
            top: 20,
            left: 60
        },
        legend: 'none',
        fontName: 'Roboto',
        backgroundColor: 'transparent'
    };
}

function _sortSensorList(sensorList) {
    return sensorList.sort((a, b) => {
        if (a.alias_name < b.alias_name) {
            return -1
        }
        else {
            return 1;
        }
    });
}