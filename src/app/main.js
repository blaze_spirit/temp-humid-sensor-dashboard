import Vue    from 'vue';
import Router from './vue-router/router.js';
import App    from './component/app.vue';
import './style.css';

new Vue({
  el: 'app',
  router: Router,
  render: h => h(App)
})