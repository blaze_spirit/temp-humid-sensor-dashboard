const UglifyJSPlugin 	 = require('uglifyjs-webpack-plugin'); 
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin  = require('copy-webpack-plugin');

module.exports = {
    entry: './src/app/main.js',
    output: {
		path: __dirname + '/dist',
        filename: './main.js'
    },
	module: {
		rules: [
			{
				test: /\.vue$/,
				loader: 'vue-loader',
				options: {
					loaders: {
						html: 'markup-inline-loader'
					}
				}
			},
			{	
				test: /\.js$/,
				exclude: /node_modules/,
				use: {
					loader: 'babel-loader',
					options: {
						presets: ['env']
					}
				}
			},
			{
				test: /\.(png|ttf)$/,
				use: {
					loader: 'url-loader',
				}
			},
			{
			    test: /\.svg/,
			    use: {
			        loader: 'svg-url-loader',
			        options: {}
			    }
			},
			{
				test: /\.css$/,
				use: [
					{ loader: "style-loader" },
					{ loader: "css-loader" }
				]
			}
		]
	},
	plugins: [
		// new UglifyJSPlugin({
		// 	test: /\.js($|\?)/i,
		// 	uglifyOptions: {
		// 		compress: true
		// 	}
		// }),

		// delete 'dist' folder on re-build.
		new CleanWebpackPlugin(['dist/']),
		
		// copy 'index.html' & 'favicon.png' to 'dist' folder.
		new CopyWebpackPlugin([
			{ from: './src/index.html', to: './' },
			{ from: './src/favicon.png', to: './' }
		])
	],
	watch: true // configure webpack to watch for changes.
};