# Overview #

Temperature & humidity sensor web dashboard.


### Set up ###

1. clone the repo.
2. run command 'npm install'
3. build using command 'webpack -p'
3. the generated file will be in the 'dist' folder

### Requirement ###

* NodeJS v8.9.1
* NPM v5.6.0